const express = require('express');
const app = express();

app.listen(3000, () => {
   console.log('app listening');
});

app.use((req, res, next) => {
    console.log('request received');
    next()
 }) 

app.get('/', (req, res) => {
   res.json({success: true});
})

app.get('/call/:status', async (req, res) => {
    let id = null;
    const status = req.params.status;
    id = await getRandomIdAsPromise()
    console.log('sending respnse');
    res.json({'success': true, id: id, status: status})
 })

 function getRandomIdAsPromise(){
    return new Promise((resolve, reject) => {
        getRandomId((ranomId) => {
            resolve(ranomId);
        })
    })
 }
 
 function getRandomId(callback) {
    setTimeout(() => {
        var random = Math.round(Math.random()*10000)
        callback(random);
    }, Math.round(Math.random()*1000))
 }

 app.get('/promise', async (req, res) => {
    let results = [];
    let random1 = await getPromise();
    results.push(random1);
    let random2 = await getPromise();
    results.push(random2);
    
    res.json({success:true, results: results})
 }) 
 
 
 function getPromise() {
    return new Promise((resolve, reject) => {
        const a = Math.random();
        resolve(a);
    })
 }
 
 